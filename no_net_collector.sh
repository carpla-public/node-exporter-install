
# !/bin/bash
sudo apt-get install wget
EXPORTER_VERSION=1.5.0
EXPORTER_PORT=17017
wget https://github.com/prometheus/node_exporter/releases/download/v$EXPORTER_VERSION/node_exporter-$EXPORTER_VERSION.linux-amd64.tar.gz
tar xvfz node_exporter-$EXPORTER_VERSION.linux-amd64.tar.gz
cd node_exporter-$EXPORTER_VERSION.linux-amd64
cp node_exporter /usr/local/bin
cd ..
rm -rf node_exporter-$EXPORTER_VERSION.linux-amd64.tar.gz
rm -rf node_exporter-$EXPORTER_VERSION.linux-amd64


cat > /etc/systemd/system/node_exporter.service << EOF
[Unit]
Description=Node Exporter
After=network.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter \
    --web.listen-address=:$EXPORTER_PORT \
    --no-collector.netclass \
    --no-collector.netdev \
    --no-collector.netstat \
    --no-collector.network_route \
    --no-collector.softnet


[Install]
WantedBy=multi-user.target
EOF

sudo useradd --no-create-home --shell /bin/false node_exporter
sudo groupadd node_exporter

sudo systemctl daemon-reload
sudo systemctl start node_exporter
sudo systemctl enable node_exporter

sudo systemctl status node_exporter --no-pager
